﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimulationWorking : MonoBehaviour
{

    public static SimulationWorking instance;

    public GameObject loadedModel;
    internal float MaxBPM;
    internal float MaxLimit;
    internal int value;
    string temp_Value1;
    string temp_Value2;
    bool firstTime;
    bool filled;
    float preValue;

    void Start()
    {
        instance = this;
        filled = false;
    }

    public void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += OnModeldetection;
    }

    public void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= OnModeldetection;
    }

    private void OnModeldetection(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
    {
        if (status)
        {
            Simulation_UIController.instance.GameUI.SetActive(true);
            loadedModel = _transform.gameObject;
            loadedModel.SetActive(false);
        }
        else
        {
            Simulation_UIController.instance._Slider.gameObject.SetActive(false);
            Simulation_UIController.instance.GameUI.SetActive(false);
            Simulation_UIController.instance._Slider.value = 0;
            Simulation_UIController.instance.anim.enabled = false;
            //Simulation_UIController.instance.text1.text = "Breathing Per Minute : ";
            //Simulation_UIController.instance.text2.text = "Volume of air per minute : ";
            Simulation_UIController.instance.EnterAge_UI.transform.localScale = Vector3.one;
            Simulation_UIController.instance.EnterAge_UI.anchorMax = new Vector2(0.5f, 0.5f);
            Simulation_UIController.instance.EnterAge_UI.anchorMin = new Vector2(0.5f, 0.5f);
            Simulation_UIController.instance.EnterAge_UI.pivot = new Vector2(0.5f, 0.5f);
            Simulation_UIController.instance.EnterAge_UI.localPosition = Vector3.zero;
            Simulation_UIController.instance.VolumeOfAir.SetActive(false);
            Simulation_UIController.instance.BreathingPerMinute.SetActive(false);
            Simulation_UIController.instance.anim.enabled = false;
            loadedModel = null;
            firstTime = false;
        }
    }

    public void slider_Activity()
    {
        if (value == 10)
        {
            MaxBPM = (Simulation_UIController.instance._Slider.value + 24) * 2.8f;
            Simulation_UIController.instance.text1.text = temp_Value1 + (Simulation_UIController.instance._Slider.value + 24);
            Simulation_UIController.instance.text2.text = temp_Value2 + (Simulation_UIController.instance._Slider.value + 24) * 0.5f;
        }
        else if (value == 19)
        {
            MaxBPM = (Simulation_UIController.instance._Slider.value + 19) * 2.8f;
            Simulation_UIController.instance.text1.text = temp_Value1 + (Simulation_UIController.instance._Slider.value + 19);
            Simulation_UIController.instance.text2.text = temp_Value2 + (Simulation_UIController.instance._Slider.value + 19) * 0.5f;
        }
        else if (value == 32)
        {
            MaxBPM = (Simulation_UIController.instance._Slider.value + 32) * 2.8f;
            Simulation_UIController.instance.text1.text = temp_Value1 + (Simulation_UIController.instance._Slider.value + 32);
            Simulation_UIController.instance.text2.text = temp_Value2 + (Simulation_UIController.instance._Slider.value + 32) * 0.5f;
        }

        if (MaxBPM >= MaxLimit)
        {
            //Vector3 pos = Simulation_UIController.instance._Slider.
            Simulation_UIController.instance.Alert.SetActive(true);
            Simulation_UIController.instance.AlertImage.gameObject.SetActive(true);
            if (preValue < Simulation_UIController.instance._Slider.value)
            {
                Debug.Log("if");
                if (value == 10)
                {
                    Simulation_UIController.instance.AlertImage.rectTransform.sizeDelta = new Vector2(10f, 25f);
                    Simulation_UIController.instance.AlertImage.fillAmount += 0.101f;
                }
                if (value == 19)
                {
                    Simulation_UIController.instance.AlertImage.rectTransform.sizeDelta = new Vector2(8f, 25f);
                    Simulation_UIController.instance.AlertImage.fillAmount += 0.14f;
                }
                if (value == 32)
                {
                    Simulation_UIController.instance.AlertImage.rectTransform.sizeDelta = new Vector2(23f, 25f);
                    Simulation_UIController.instance.AlertImage.fillAmount = (Simulation_UIController.instance._Slider.value-32) * 0.076f;
                    //Simulation_UIController.instance.AlertImage.fillAmount += 0.043f;
                }
                preValue = Simulation_UIController.instance._Slider.value;
            }
            else
            {
                Debug.Log("else");
                if (value == 10)
                {
                    Simulation_UIController.instance.AlertImage.rectTransform.sizeDelta = new Vector2(10f, 25f);
                    Simulation_UIController.instance.AlertImage.fillAmount -= 0.101f;
                }
                if (value == 19)
                {
                    Simulation_UIController.instance.AlertImage.rectTransform.sizeDelta = new Vector2(8f, 25f);
                    Simulation_UIController.instance.AlertImage.fillAmount -= 0.14f;
                }
                if (value == 32)
                {
                    Simulation_UIController.instance.AlertImage.rectTransform.sizeDelta = new Vector2(23f, 25f);
                    Simulation_UIController.instance.AlertImage.fillAmount = (Simulation_UIController.instance._Slider.value + 32) * 0.076f;
                    //Simulation_UIController.instance.AlertImage.fillAmount -= 0.043f;
                }
                preValue = Simulation_UIController.instance._Slider.value;
            }
            Debug.Log("filled:" + Simulation_UIController.instance._Slider.direction);
        }
        else
        {
            preValue = 0;
            Simulation_UIController.instance.Alert.SetActive(false);
            Simulation_UIController.instance.AlertImage.fillAmount = 0f;
            Simulation_UIController.instance.AlertImage.gameObject.SetActive(false);
        }
    }

    public void AgeGroup(int x)
    {
        value = x;
        Debug.Log(value);
        MaxLimit = (220 - x) * 0.8f;
        temp_Value1 = "Breathing Per Minute : ";
        temp_Value2 = "Volume of air per minute : ";
        Simulation_UIController.instance._Slider.value = 0;

        if (!firstTime)
        {
            Simulation_UIController.instance.anim.enabled = true;
            Simulation_UIController.instance._Slider.gameObject.SetActive(true);
            Simulation_UIController.instance.VolumeOfAir.SetActive(true);
            Simulation_UIController.instance.BreathingPerMinute.SetActive(true);
            firstTime = true;
            loadedModel.SetActive(true);
        }

        if (value == 10)
        {
            Simulation_UIController.instance.text1.text = temp_Value1 + 24;
            Simulation_UIController.instance.text2.text = temp_Value2 + 24 * 0.5f;
        }
        else if (value == 19)
        {
            Simulation_UIController.instance.text1.text = temp_Value1 + 19;
            Simulation_UIController.instance.text2.text = temp_Value2 + 19 * 0.5f;
        }
        else if (value == 32)
        {
            Simulation_UIController.instance.text1.text = temp_Value1 + 32;
            Simulation_UIController.instance.text2.text = temp_Value2 + 32 * 0.5f;
        }
    }
}
