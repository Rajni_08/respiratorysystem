﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespiratoryModelHandler : MonoBehaviour {

	public GameObject noseIn;
	public GameObject noseOut;

	public GameObject[] CO2;
	public GameObject[] O2;

	
	float timer;
	bool isInhaling;

	bool isInfoOpen;
	bool isHelpOpen;
	bool isIntroOpen;

	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		timer = timer + Time.deltaTime;
		if (timer > 1) {
			isInhaling = !isInhaling;
			timer = 0;
		}

		if (isInhaling) {
			noseIn.SetActive (true);
			noseOut.SetActive (false);
			for (int i = 0; i < CO2.Length; i++) {
				CO2 [i].SetActive (true);
				O2 [i].SetActive (false);
			}
		} else {
			noseIn.SetActive (false);
			noseOut.SetActive (true);
			for (int i = 0; i < CO2.Length; i++) {
				CO2 [i].SetActive (false);
				O2 [i].SetActive (true);
			}
		}
	}	
}
