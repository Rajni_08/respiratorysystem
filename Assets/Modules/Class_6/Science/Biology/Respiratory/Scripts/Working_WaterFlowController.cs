﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Working_WaterFlowController : MonoBehaviour {

    public List<Items> _items;

    void OnDisable() {
        for (int i = 0; i < _items.Count; i++)
        {
            _items[i].waterMaterial.mainTextureOffset = Vector2.zero;
        }
    }

	// Update is called once per frame
	void Update () {
        for (int i = 0; i < _items.Count; i++)
        {
            //if (_items[i].rotate90Degree) {
                _items[i].waterMaterial.mainTextureOffset = new Vector2(_items[i].waterMaterial.mainTextureOffset.x + _items[i].waterSpeed * Time.deltaTime, 0);
            //}
            //else {
            //    _items[i].waterMaterial.mainTextureOffset = new Vector2(0, _items[i].waterMaterial.mainTextureOffset.y - _items[i].waterSpeed * Time.deltaTime);
            //}
        }
	}
}

[System.Serializable]
public class Items : System.Object {
    public Material waterMaterial;
    public float waterSpeed;
    public bool rotate90Degree;
}
