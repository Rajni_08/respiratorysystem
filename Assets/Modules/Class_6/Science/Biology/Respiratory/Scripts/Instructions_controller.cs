﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instructions_controller : MonoBehaviour {


    void Start()
    {
        StartCoroutine(startActivity());
    }

    private IEnumerator startActivity()
    {
        yield return new WaitForSeconds(3f);
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(true);
    }

    public void ButtonInfo(string arg)
    {
        Respiratory_Controller.instance.btn_info(arg);
    }
}
