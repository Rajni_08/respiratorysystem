﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Simulation_UIController : MonoBehaviour {

    public static Simulation_UIController instance;

    public Slider _Slider;
    public Text text1;
    public Text text2;
    public GameObject Alert;
    public GameObject GameUI;
    public RectTransform EnterAge_UI;
    public GameObject VolumeOfAir;
    public GameObject BreathingPerMinute;
    public Animation anim;
    public Image AlertImage;
    public Image PersonImage;


    void Start()
    {
        instance = this;    
    }
}
