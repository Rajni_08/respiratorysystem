﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Working_Controller : MonoBehaviour {

    public static Working_Controller instance;

    public List<GameObject> loadedModel;

    void Start()
    {
        instance = this;
    }

    public void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += OnModeldetection;
    }

    public void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= OnModeldetection;
    }

    private void OnModeldetection(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
    {
       if(status)
        {
            Worikng_UIController.instance.ImportanceOfEpiglosttics.SetActive(true);

            if (_transform.GetChild(0).transform.GetChildCount() > 0)
            {
                for (int i = 0; i < _transform.GetChild(0).transform.GetChildCount(); i++)
                {
                    loadedModel.Add(_transform.GetChild(0).transform.GetChild(i).gameObject);
                }
            }
            Worikng_UIController.instance.loadingImage.SetActive(true);
        }
        else
        {
            loadedModel.Clear();
            Worikng_UIController.instance.loadingImage.SetActive(false);
            Worikng_UIController.instance.ImportanceOfEpiglosttics.SetActive(false);
            Worikng_UIController.instance.RemoveEpiglottis.SetActive(false);
            Worikng_UIController.instance.NormalFlow.SetActive(false);
        }
    }

    public void ImportanceOfEpiglottis()
    {
        Worikng_UIController.instance.loadingImage.SetActive(false);
        Worikng_UIController.instance.ImportanceOfEpiglosttics.SetActive(false);
        Worikng_UIController.instance.NormalFlow.SetActive(true);
        loadedModel[0].transform.GetChild(0).gameObject.SetActive(false);
        loadedModel[4].SetActive(true);
        Worikng_UIController.instance.RemoveEpiglottis.SetActive(true);
        StartCoroutine(ReverseAnim());
    }

    private IEnumerator ReverseAnim()
    {
        yield return new WaitForSeconds(4f);
        loadedModel[4].GetComponent<Animator>().Play("NormalFlow 1", 0);
    }

    public void RemoveEpiglottis()
    {
        Worikng_UIController.instance.RemoveEpiglottis.SetActive(false);
        Worikng_UIController.instance.NormalFlow.SetActive(false);
        loadedModel[1].transform.GetChild(0).gameObject.SetActive(false);
        loadedModel[4].SetActive(false);
        loadedModel[3].SetActive(true);
        loadedModel[2].transform.GetChild(25).GetComponent<Animator>().Play("WaterMesh", 0);
    }

    public void Cross()
    {
        loadedModel[1].transform.GetChild(0).gameObject.SetActive(true);
        loadedModel[2].transform.GetChild(25).GetComponent<Animator>().Play("WaterMesh 0",0);
        StartCoroutine(completed());
    }

    private IEnumerator completed()
    {
        yield return new WaitForSeconds(2.5f);
        loadedModel[3].SetActive(false);
        //Worikng_UIController.instance.RemoveEpiglottis.SetActive(true);
        Worikng_UIController.instance.loadingImage.SetActive(true);
        Worikng_UIController.instance.ImportanceOfEpiglosttics.SetActive(true);
        loadedModel[0].transform.GetChild(0).gameObject.SetActive(true);
    }
}
