﻿using UnityEngine;
using System.Collections;

public class RespiratoryInfo : MonoBehaviour
{

	public static string[,] info = new string[,] { {
            "Epiglottis",
            "The epiglottis is a leaf-shaped flap of cartilage located behind the tongue, at the top of the larynx, or voice box. The main function of the epiglottis is to seal off the windpipe during eating, so that food is not accidentally inhaled."
        }, {
			"Alveoli",
			"Alveoli are tiny sacs within our lungs that allow oxygen and carbon dioxide to move between the lungs and bloodstream. Learn more about how they function and quiz your knowledge at the end."
		}, {
			"Lungs",
			"The lungs are the primary organs of respiration in humans and many other animals including a few fish and some snails. In mammals and most other vertebrates, two lungs are located near the backbone on either side of the heart."
		}, {
			"Pharynx",
			"The pharynx is the part of the throat that is behind the mouth and nasal cavity. These tubes going down to the stomach and the lungs."
		}, {
			"Trachea",
			"The trachea is colloquially called the windpipe. It is jointly originated with foodpipe from the mouth but separates from foodpipe through Epiglottis."
		}
	};
	public static string[] heading = new string[] {
        "Epiglottis",
		"Alveoli",
		"Lungs",
		"Pharynx",
		"Trachea"
	};
}
