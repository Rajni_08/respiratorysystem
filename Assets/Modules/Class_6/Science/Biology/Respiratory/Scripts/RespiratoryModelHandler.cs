﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationModelHandler : MonoBehaviour {

    public GameObject noseIn;
    public GameObject noseOut;



    float timer;
    bool isInhaling;

    bool isInfoOpen;
    bool isHelpOpen;
    bool isIntroOpen;


    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        timer = timer + Time.deltaTime;
        if (timer > 1) {
            isInhaling = !isInhaling;
            timer = 0;
        }

        if (isInhaling) {
            noseIn.SetActive(true);
            noseOut.SetActive(false);
        }
        else
        {
			noseIn.SetActive(false);
			noseOut.SetActive(true);
        }
	}	
}
