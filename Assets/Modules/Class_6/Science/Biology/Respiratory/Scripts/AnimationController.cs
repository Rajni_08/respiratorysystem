﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationController : MonoBehaviour {

    private Animator anim;
    public Slider slider;   //Assign the UI slider of your scene in this slot 
                            // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.speed = 0;
    }
    // Update is called once per frame
    void Update()
    {
        anim.Play("Person", 0, slider.normalizedValue);
    }
}
