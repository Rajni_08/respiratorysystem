﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvexAndModuleHandler : MonoBehaviour
{

	public GameObject[] imageTargets;
    public GameObject[] canvexUI;
    public GameObject[] imageTargetChild;

    void OnEnable ()
	{
	//	DefaultTrackableEventHandler.onImageTargetDetected += DefaultTrackableEventHandler_onImageTargetDetected;
	}


	void Disable ()
	{
	//	DefaultTrackableEventHandler.onImageTargetDetected -= DefaultTrackableEventHandler_onImageTargetDetected;
	}

	void DefaultTrackableEventHandler_onImageTargetDetected (GameObject gameObj, bool isFound)
	{
        if (isFound && gameObj != null)
        {
            for (int i = 0; i < imageTargets.Length; i++)
            {
                Debug.Log("gameobject " + gameObj.ToString());
                if (gameObj.ToString().ToLower().Equals(imageTargets[i].ToString().ToLower()))
                {
                    if (canvexUI[i] != null)
                        canvexUI[i].SetActive(true);
                    if (imageTargetChild.Length > 0&&imageTargetChild[i] != null)
                        imageTargetChild[i].SetActive(true);
                }
                else
                {
                    Debug.Log("canvex false");
                    if (canvexUI[i] != null)
                        canvexUI[i].SetActive(false);
                    if (imageTargetChild.Length > 0&& imageTargetChild[i] != null)
                        imageTargetChild[i].SetActive(false);
                }
            }
        }
        else {
            for (int i = 0; i < imageTargets.Length; i++)
            {
                if(canvexUI[i]!=null)
                    canvexUI[i].SetActive(false);
                if (imageTargetChild.Length>0 && imageTargetChild[i] != null)
                    imageTargetChild[i].SetActive(false);

            }
         }
	}



}
