﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoController : MonoBehaviour {

	#region private variables.
	   private bool isVideoPlay;
	   private int storeActiveUIPosition=-1;
	   private GameObject currentActiveObject, planeObject;

	#endregion

	#region public variables.
	   public GameObject videoButton, videoPlayPlane;
	   public GameObject[] canvasObject;
	   public AudioSource[] audioSource;
	   public Sprite[] sprite;
	   public VideoClip videoClip;
	#endregion

	private void OnEnable()
	{
		//_ImageTargetDetected.onImageTargetDetected += OnTargetDetection;

	}

	private void OnDisable()
	{
		//_ImageTargetDetected.onImageTargetDetected -= OnTargetDetection;
	}


	void OnTargetDetection(GameObject @object,  bool status)
	{
		currentActiveObject = @object;
		GameObjectUIOffOn(videoButton, status);
		isVideoPlay = false;
		storeActiveUIPosition = -1;
		
		TargetOnOff(!status);
		if (videoButton!=null && sprite.Length>0)
		    videoButton.transform.GetChild(0).GetComponent<Image>().overrideSprite = sprite[1];
		if (planeObject!=null)
		   Destroy(planeObject);
	}

	public void VideoOnOff()
	{
		isVideoPlay = !isVideoPlay;
		CanvasUIOnOff(isVideoPlay);
		for (int i = 0; i < audioSource.Length; i++)
			audioSource[i].mute = isVideoPlay;

		if (currentActiveObject!=null)
		{

			TargetOnOff(isVideoPlay);
			if (planeObject!=null)
			   planeObject.SetActive(isVideoPlay);

			if (!currentActiveObject.transform.Find("VideoPlane")&& videoClip!=null) {
				planeObject = Instantiate(videoPlayPlane, currentActiveObject.transform,false);
				planeObject.name = "VideoPlane";
				planeObject.GetComponent<VideoPlayer>().clip = videoClip;
			}
			//planeObject.GetComponent<VideoPlayer>().time = 0f;
			planeObject.GetComponent<VideoPlayer>().Play();
			planeObject.GetComponent<VideoPlayer>().isLooping = true;

			if (isVideoPlay)
			{
				videoButton.transform.GetChild(0).GetComponent<Image>().overrideSprite = sprite[0];
			}
			else
			{
				videoButton.transform.GetChild(0).GetComponent<Image>().overrideSprite = sprite[1];
			}

		}

	}

	void GameObjectUIOffOn(GameObject @object, bool status)
	{
		if (@object!=null)
		{
			Debug.Log(status);
			@object.SetActive(status);
		}
	}

	void TargetOnOff(bool status)
	{
		for (int i = 0; i < currentActiveObject.transform.childCount; i++)
		{
			currentActiveObject.transform.GetChild(i).gameObject.SetActive(!status);
		}
	}  


	void CanvasUIOnOff(bool status)
	{
		for (int i = 0; i < canvasObject.Length; i++)
		{
			if (status && canvasObject[i].activeInHierarchy)
			{
				storeActiveUIPosition = i;
				GameObjectUIOffOn(canvasObject[i], !status);
			}
			else  if(storeActiveUIPosition==i)
			{
				GameObjectUIOffOn(canvasObject[storeActiveUIPosition], !status);
			}
		}
	}
}
					