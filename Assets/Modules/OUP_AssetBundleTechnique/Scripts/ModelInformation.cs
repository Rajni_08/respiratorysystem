﻿using UnityEngine;

[SerializeField]
public class ModelInformation
{
	public string AssetName = "";
	public string AssetId = "";
	public string AssetTypeId = "";
	public string AssetTypeName = "";
	public string AssetSubTypeId = "";
	public string AssetSubTypeName = "";
	public string SubCategory = "";
	public string UserId = "";
	public string OrganizationId = "";
	public string OrganizationName = "";
	public string ScheduledUserId = "";
	public string ClassId = "";
	public string ClassName = "";
	public string SectionId = "";
	public string SectionName = "";
	public string SubjectId = "";
	public string SubjectName = "";
	public string SessionId = "";
	public string PlatformType = "";
}
