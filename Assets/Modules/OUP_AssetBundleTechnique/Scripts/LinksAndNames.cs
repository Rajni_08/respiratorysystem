﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class LinksAndNames
{
	public static string assetBundleDynamicUrl = "";

	public static AssetBundleManifest manifestAssetBundle;

	public static string StoreAddress = "http://onelink.to/5ku3xe";

	public static string LinkToServer = "http://dev.wizar.io/";

	public static string LinkMenuJson = LinkToServer + "kidsMobile/json";

	public static string LinkToActivation = LinkToServer + "kidsMobile/activate";

	public static string LinkToPdfDownload = LinkToServer + "kidsMobile/download/pdf";

	public static string LinkToAssetBundle = LinkToServer + "kidsMobile/download/assetBundle";

	public static string LinkToRegisterIAP = LinkToServer + "kidsMobile/buyPdf";

	public static string LinkToSignUp = LinkToServer + "kidsMobile/saveUserInfo";

	public static string LinkToSignIn = LinkToServer + "kidsMobile/login";

	public static string LinkToForgotPassword = LinkToServer + "kidsMobile/fpassword";

	public static string LinkToImages = "http://wizario-local.s3-ap-southeast-1.amazonaws.com/Mobile/" + Application.identifier + "/images/";

	//public static string LinkToImages = "http://wizario-dev.s3-ap-southeast-1.amazonaws.com/Mobile/" + Application.identifier + "/images/";

	public static string FolderImages = Application.persistentDataPath + "/" + Application.productName + "/Images/";

	public static string FolderPdf = Application.persistentDataPath + "/" + Application.productName + "/pdf/";

	public static string twitterURL = "https://twitter.com/wizarlearning";

	public static string facebookURL = "https://www.facebook.com/wizarkids";

	public static string youtubeURL = "https://www.youtube.com/channel/UCuRsWmb7o4ILp2r4koxSHsQ";

	public static string tutorialURL = "https://youtu.be/KIJ6A_k50Pw";

	public static string Warning01 = "Something went wrong. Start without Updating ?";

	public static string Warning02 = "Something went wrong, restart downloading ?";

	public static string Warning03 = "Something went wrong, restart loading ?";

	public static string error01 = "You have started wizar directly! Close and start from mybag or myclass Application";

	public static string error02 = "Could not connect to server, please check your internet connection.";

	public static string error03 = "Server is under maintenance, please be patient and try after some time.";

	public static string error04 = "Wrong Activation Key. please check and re-enter the key.";

	public static string error05 = "This Activation key is not for the book you are trying to activate. Thankyou.";

	public static string error06 = "Unable to process your request, please try after some time.";

	public static string error07 = "Unknown Error, Please restart your application or redownload application.";

	public static string error08 = "Your application is outdated, please update your app from store.";

#if UNITY_ANDROID
	public static string PathToScreenshots = "/mnt/sdcard/DCIM/WizAR/";
	public static string PathToScreenshotsThumbnail = "/mnt/sdcard/DCIM/WizAR/";
#endif

#if UNITY_IOS
	public static string PathToScreenshots = Application.persistentDataPath + "/WizAR/";
	public static string PathToScreenshotsThumbnail = Application.persistentDataPath + "/WizAR/";
#endif


}