﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RS_LogoScript : MonoBehaviour {

    public GameObject LeftMenuPanel;
    public GameObject Wizar_pnl;
    public GameObject btnBack;
    public GameObject wizarLogo;


    public void WizArLogoManager()
    {
        wizarLogo.SetActive(true);
        ActivateUiOnCapture(false);
        Invoke("DisableWizARLogo", 4);
    }

    void DisableWizARLogo()
    {
        wizarLogo.SetActive(false);
        ActivateUiOnCapture(true);
    }

    void ActivateUiOnCapture(bool status)
    {
        btnBack.SetActive(status);
        LeftMenuPanel.SetActive(status);
        Wizar_pnl.SetActive(status);
    }
}
