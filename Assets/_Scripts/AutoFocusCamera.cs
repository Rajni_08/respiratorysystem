﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class AutoFocusCamera : MonoBehaviour
{

	void Start ()
	{
		VuforiaBehaviour.Instance.OnEnableEvent += OnVuforiaStarted;
		VuforiaBehaviour.Instance.OnApplicationPauseEvent += OnPaused;
		StartCoroutine ("ContinuesFocus");
	}

	private IEnumerator ContinuesFocus(){
		yield return new WaitForSeconds (1.0f);
		CameraDevice.Instance.SetFocusMode (
			CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
	}

	private void OnVuforiaStarted ()
	{
		CameraDevice.Instance.SetFocusMode (
			CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
	}

	private void OnPaused (bool paused)
	{
		if (!paused) { // resumed
			// Set again autofocus mode when app is resumed
			CameraDevice.Instance.SetFocusMode (
				CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		}
	}

}
