﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.SceneManagement;

static public class AssetBundleDownloader {
	// A dictionary to hold the AssetBundle references
	static private Dictionary<string, AssetBundleRef> dictAssetBundleRefs;

	static AssetBundleDownloader (){
		dictAssetBundleRefs = new Dictionary<string, AssetBundleRef>();
	}
	// Class with the AssetBundle reference, url and version
	private class AssetBundleRef {
		public AssetBundle assetBundle = null;
		public string url;
		public AssetBundleRef(string strUrlIn) {
			url = strUrlIn;
		}
	};
	// Get an AssetBundle
	public static AssetBundle getAssetBundle (string url){
		string keyName = url;
		AssetBundleRef abRef;
		if (dictAssetBundleRefs.TryGetValue(keyName, out abRef))
			return abRef.assetBundle;
		else
			return null;
	}
	// Download an AssetBundle
	public static IEnumerator downloadAssetBundle (string url){
		string keyName = url;
		if (dictAssetBundleRefs.ContainsKey(keyName))
			yield return null;
		else {
			using(WWW www = WWW.LoadFromCacheOrDownload (url,0)){
				yield return www;
				if (www.error != null)
					throw new Exception("WWW download:" + www.error);
				AssetBundleRef abRef = new AssetBundleRef (url);
				abRef.assetBundle = www.assetBundle;
				dictAssetBundleRefs.Add (keyName, abRef);
			}
		}
	}
//
//	static void Apply( )
//	{
//		Texture2D texture = Selection.activeObject as Texture2D;
//		SceneManager manager = Scene as SceneManager;
//		( texture == null )
//		{
//			EditorUtility.DisplayDialog( "Select Texture", "You must select a texture first!", "OK" );
//			return;
//		}
//
//		string path = EditorUtility.SaveFilePanel( "Save png", "", texture.name + ".png", "png" );
//		if( path.Length != 0 )
//		{
//			// Convert the texture to a format compatible with EncodeToPNG
//			if( texture.format != TextureFormat.ARGB32 && texture.format != TextureFormat.RGB24 )
//			{
//				Texture2D newTexture = new Texture2D( texture.width, texture.height );
//				newTexture.SetPixels( texture.GetPixels( 0 ), 0 );
//				texture = newTexture;
//			}
//
//			byte[] pngData = texture.EncodeToPNG( );
//			if( pngData != null )
//				File.WriteAllBytes( path, pngData );
//		}
//	}

	// Unload an AssetBundle
	public static void Unload (string url, int version, bool allObjects){
		string keyName = url + version.ToString();
		AssetBundleRef abRef;
		if (dictAssetBundleRefs.TryGetValue(keyName, out abRef)){
			abRef.assetBundle.Unload (allObjects);
			abRef.assetBundle = null;
			dictAssetBundleRefs.Remove(keyName);
		}
	}
}